# Freedom Cells website

[PaperMod][https://github.com/adityatelange/hugo-PaperMod/wiki)

## Content

New article:

```
hugo new --kind post posts/<title>.md
```

## Development

Updating theme:

```
git submodule update --remote --merge
```

Starting container:

```
docker-compose up -d
```

Build PDF documents:

- Update docker-compose.yml (TODO)

```
docker-compose up --build -d
```
