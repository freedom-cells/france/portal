#!/bin/bash

set -e

gen() {
  local lang="$1"
  local slug="freedom-cell-manual"
  local input="content/posts/$slug.$lang.md"
  local output="static/$slug.$lang.pdf"

  local title="$(grep '^title:' content/posts/freedom-cell-manual.$lang.md | tail -n1)"
  title=$(echo "${title//title: /}" | tr -d '"')

  local desc="$(grep '^description:' content/posts/freedom-cell-manual.$lang.md | tail -n1)"
  desc=$(echo "${desc//description: /}" | tr -d '"')

  local head="<style>$(cat ".docker/pandoc.css")</style>"
  local content="$(grep -v title: "$input")"
  local html="${head}$(echo "$content" | pandoc -t html)"

  local xsl=".docker/tmp-toc.xsl"
  sed -e "s/\${TITLE}/$title/g" \
    -e "s/\${DESC}/$desc/g" \
    ".docker/toc.xsl" > "$xsl"

  # --disable-smart-shrinking --dpi 96 \
  # --xsl-style-sheet tocfile.xsl
    #--enable-smart-shrinking \
  # grep -v title: "$input" | pandoc -s -t html
  echo "$html" | wkhtmltopdf --quiet \
    --encoding UTF-8 --print-media-type \
    --margin-top 20mm --margin-bottom 20mm --margin-left 15mm --margin-right 15mm \
    --title "$title" \
    toc \
    --toc-header-text "$title" \
    --xsl-style-sheet "$xsl" \
    --toc-level-indentation 2em \
    --toc-text-size-shrink 1 \
    - \
    --footer-font-size 9 \
    --footer-right "[page]" \
    --footer-spacing 5 \
    "$output"

  echo "Generated $output"
}

for lang in en fr; do
  gen "$lang"
done
