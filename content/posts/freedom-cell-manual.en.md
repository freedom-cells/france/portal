---
title: "Freedom Cell Manual"
# date: 2021-12-16T09:45:03+01:00
tags:
- Guide
showToc: true
TocOpen: true
description: "Note that we are learning as we go and some strategies may work better for some groups than others. One of the goals of the Freedom Cell Network is to be a living institution that is self correcting and evolving. As individuals and groups learn best practices they are encouraged to share those with the network as a whole so other cells can learn from them and become more effective."
pdf: /freedom-cell-manual.en.pdf
---

## What is a Freedom Cell?

Freedom Cells are small groups of 8 - 10 people working together for mutual aid, achieving common goals, and securing the sovereignty of group members.

The cells then link up with other groups in their area to create a mid-size network (middle cadre) and then those networks link up to create a larger network (meta cadre).

As the network grows in size so too does it's ability to support participating members and achieve common goals.

The hope is Freedom Cell Network grows large enough and effective enough to one day replace the state as a means of social organization.

## Principles of The Freedom Cells Network

1. Localization
2. Decentralization
3. Solutions focused
4. Apolitical
5. Non violent

## Why Are Freedom Cells Necessary?

Introduction from Derrick Broze's book, [How to Opt Out of the Technocratic State](https://theconsciousresistance.com/howto/):

> As humanity enters the second decade of the 21st century, we find ourselves at the precipice of a Technocratic Age where Artificial Intelligence (AI), Smart Technology, and the Internet of Things are becoming a part of everyday life. This technology provides benefits but comes at a cost—corporations, governments, law enforcement, and hackers are all capable of peering into our lives at any moment. Corporations and governments are even learning to use technology in a way that allows them to be the "social engineers" of society. The concept of social credit is also becoming increasingly popular, and the likelihood that citizens will face negative consequences for choosing to speak about controversial topics or criticizing authorities is only going to increase.
>
> This shift toward a world where digital technology is the solution for all things is being driven by the tech sector—specifically the institutions often referred to as Big Wireless and Big Tech. The CEOs of transnational corporations and their partners in government have worked to cement digital technology into every aspect of humanity. The world they envision is one where scientists and technologists are the elite class who decide the future of society. While the digital technology of these industries has only emerged in the last few decades, the philosophy which guides many of the leading figures in industry and government is nearly a century old. This philosophy of a rule by technological experts and scientists is known as Technocracy. As we will see in the coming chapters, the ideas which underpin this school of thought have quietly been influencing world leaders for decades.
>
> Is this obscure political theory from the 20th century the guiding force behind the move towards a digital dystopia? What are the implications for a world that is always plugged in and on "the grid"? How can one maintain privacy and liberty in a society that is based on mass surveillance, technological control, and the loss of individuality?

## Organizing Freedom Cells

**Inner Cadre** (personal)<br>
8 – 1 1 1 1 1 1 1 1

**Middle Cadre** (local)<br>
64 – 8 8 8 8 8 8 8 8

**Meta Cadre** (regional)<br>
512 – 64 64 64 64 64 64 64 64

**Confederation of Freedom Cells**<br>
4096 – 512 512 512 512 512 512 512 512

### The Inner Cadre

The foundation of the Freedom Cell Network are the small groups of eight people known as inner cadres. The members of these groups voluntarily associate with one another and commit to support one another through mutual aid and reciprocity. The groups should create a vision and mission statement, establish common goals, and meet regularly, at least once a month, to check in with one another.

### Why eight people?

The number eight comes from the work of John David Garcia and Bob Podolsky. In 2005, Garcia published a book titled [Creative Transformation: A Practical Guide for Maximizing Creativity](http://see.org/garcia/e-ct-dex.htm). Garcia believed that creativity was one of the most important characteristics an individual or group could strive for. After much research, Garcia found that eight people is the optimal number for a group to be the most creative.

> Couples are usually too small a group. They find it easy to accept common delusions. Still, we are better off in a couple than we are alone. Groups larger than eight soon begin to create a hierarchy whose members find it difficult to treat each other with equal respect and to communicate on a personal level, soul to soul. It becomes almost impossible for groups larger than fifteen. Larger groups should be broken down into smaller groups, preferably of eight each. Therefore, if the process is to be optimized, it should involve four men and four women who freely choose each other and agree to work together to become creatively transformed. Persons who have difficulty creating their own groups of eight may expect help in forming groups of eight from those who have already begun the process. [http://see.org/garcia/e-ct-5.htm](http://see.org/garcia/e-ct-5.htm)

This number of eight is something we strive for in the Freedom Cell Network however it is not an absolute. The aim is for the inner cadre group to have as close to eight people as possible. And while a split between men and women is ideal, it is also not an absolute.

## 12 Tips For Building Freedom Cells

From Derrick Broze's book, [How to Opt Out of the Technocratic State](https://theconsciousresistance.com/howto/):

> In conclusion, I offer these "**12 Tips For Building Freedom Cells**" as a starting point for launching your group. Please adapt these to the specific needs of your community:
>
> 1. Understand Your Motivation - I find it valuable for every person considering starting a cell/circle/hub to know why they are pursuing such a goal. What are your motivations and interests? Knowing this before you start a group will save you time. Finding ways to opt-out of the Technocracy is an obvious goal, but what else drives you?
> 2. Identify Potential Candidates - Are they mentally, physically, spiritually sound for your goals?
> 3. Discuss Common Themes - What are the driving forces bringing the group together?
> 4. Identify Strengths and Weaknesses - Take an honest look at the strengths and weaknesses of each individual as well as the group as a whole.
> 5. Evaluate Desired Level of Freedom vs Security - Every individual may have a different desired level of freedom and as such will have different aims and acceptability of risks. When it comes to the Technocracy this is especially important to remember. How free do you really want to be? How much privacy do you want to keep? What will you do to attain such a goal?
> 6. Set Short Term and Long Term Goals - What can your cell accomplish in three months? Six months? A year? Set goals as a group and hold each other accountable.
> 7. Mindfulness Training - Incorporate practices like Nonviolent Communication Training and group meditation into your cell.
> 8. Accomplish Goals - Document each goal successfully met by the cell or individual members.
> 9. Ongoing Group Education, Communication - Continuously expand your cell's knowledge, skills, and supplies.
> 10. Promote/Market Goals and Accomplishments - Use the power of social media (when safe) and marketing to let the world know how much more prosperous you are in the counter-economy.
> 11. Identify Strategies For Creating Income/Independence - Leverage the power and number of your cell to create counter-economic income that cannot be taxed by the State.
> 12. Network with Other Cells - The key to opting-out of the Technocratic State is building the counter-economic community. This means not only your immediate community of allies but the larger network of cells in your city, state/province, nation, and the global community. It is up to you to make an effort to network with other activists and free thinkers.

## Organizing the Inner-Cadre

In this section we will discuss forming the inner cadre from three different starting points.

1. An individual without anyone in a group.
2. A group with 2 to 6 people.
3. More than eight people in a group.

Next we will share some important tips about security culture, specifically how to ensure you are not working closely with a government agent or troubled person who undermines the progress and cohesion of the group.

### An individual without anyone in a group - Lone Wolf

The work of the "lone wolf" in forming an inner cadre group is perhaps the most difficult. The first step, if not already done, is to join the Freedom Cells website at [https://freedomcells.org](https://freedomcells.org). From there you will be able to use the member and cell maps to see if there are any members or a cell in your area. If there are, message them and/or join the cell. Not everyone has that luxury however.

If there is not anyone already in your area, it is up to you to begin building the Freedom Cell Network from the ground up. Congratulations!

### Here's where to start

Begin with people you already know and trust that are on the same page. Think concentric circles with you at the center of the circle then those closest to you in the next circle, and those you are acquaintances with in the next circle, and people you have only met a couple times in the next circle. Start with the inner circles.

*Remember, not everyone in the Freedom Cell Network has to be radical anarchists or share the
same views 100%.*

If you have someone close to you interested in prepping, gardening, or that speaks negatively about government and the status quo, they may be a good start.

If there is no one you know already that would be a good fit, there are places and outlets most communities have which are ripe for recruitment. Digitally, you can use Meetup.com or Facebook.com to find meetups or groups in your area that are of like mind. Areas to start would be libertarian meetups, prepper/survivalist groups, gardeners and farmers, homeschooling, unschooling, gun rights groups, police accountability groups, and any group that is working to either opt out of the status quo or is clearly fed up with the status quo and looking for solutions.

Create a MeetUp or other social media group to attract people. For example, a Voluntary and/or Agorist group, or a gardening, canning, prepping, etc group for your area. Use this as a recruiting platform. Hold regular meetings focused on Voluntary/Agorist topics. This will create a good pool of people to add to your cell once vetted. Keep these meetings separate from your inner cadre meetings. These should be welcoming to the public and topics discussed accordingly.

### Other resources for creating groups

Nextdoor app * Telegram

Search for groups and activities in your area that appeal to your interests that would fit well with a cell (gardening, permaculture, HAM or CB radio, food preservation, etc).

Another fine place to meet people is at a local farmers market. Here you will find people that are at least thinking outside the box when it comes to where their food is sourced, a key facet of independent living.

Once you find people that may be good people to work with on this endeavour, introduce yourself. Get to know them. Try to build "**shared value, based on shared values**". Once you are comfortable with one another, let them know you are looking to grow small mutual aid groups and ask them if they would be interested in becoming part of your group.

Know that this process of building trust and finding the right people from scratch could take time. Be patient and trust that the right people will come into your life.

Note however, if it is too difficult because you live in a small town or an area where people generally do not value freedom, while it may be a difficult step, consider moving somewhere that has a higher concentration of people that value liberty. The Freedom Cells site would be a great place to research the best area for you to move to.

You can preemptively link up with other Freedom Cell members and get advice on good areas and what to be aware of when moving to their area.

### A group with 2 to 6 people

If you already have two or more people in your group but not yet a full eight, you can utilize many of the same methods listed above to recruit your final members. You can multiply your effectiveness by dividing up parts of town or different focus areas. For example, one group member can focus on recruiting from farmers markets while another member can start building relationships with the local homeschooling groups. Having more than one person in your group will make it easier to add new people as those considering joining will see there is already some momentum with the idea.

However, another strategy that would rapidly advance the development of a middle cadre (eight groups of around eight people) in your area would be for each of those people in your group to set the goal of recruiting seven people. In this regard, each of the first members form a recruitment cell of sorts where they work together, sharing best practices, while they individually reach out to their own networks in hopes of building multiple cells right off the bat.

A group of less than eight people can also leverage the FreedomCells.org member map to attempt to link up with other FC members in your area.

### More than eight people in a group

Having more than eight people in a group when you begin on this journey is a great place to be. When you have more than 8 people you essentially have the beginning of the middle cadre (8 groups of 8) already in place. From here you can work down to build the inner cadre groups. Now, the question naturally arises, "how do we split up into two or more groups from here?" Remember, as John David Garcia correctly pointed out, "Groups larger than eight soon begin to create a hierarchy whose members find it difficult to treat each other with equal respect and to communicate on a personal level, soul to soul".

Make sure everyone understands the purpose of splitting the larger group into smaller groups. This will help to amplify creativity and increase the effectiveness of communication and mutual aid. Also know that the whole group will still be connected through the middle cadre group although the inner cadre members are encouraged to form tighter bonds and connections with one another than with the broader middle cadre group.

## Methods of Organization

There are various methods to use when deciding how to split larger groups into smaller ones. What follows is a brief list and description of four methods you may utilize in order to organize the middle cadre. Note that some of these methods may work better for some groups than others and it is possible that you may find that multiple strategies are needed to organize your middle cadre network.

### Familiarity and trust

One of the best ways to organize inner cadres is by linking up with those whom you already have a familiar and trusting relationship. In this regard you can get right down to business without having to "vet" them or worry about security issues. This does not always work for the entire group however as not everyone may know one another closely. In this case those who are familiar or family with one another can create inner cadres and the group as a whole can utilize one of the other methods to organize the rest of the middle cadre group.

### Geographic proximity

While inner cadre groups do not need to be "meet in person" groups and can be virtual, for the purpose of accomplishing goals and protecting the freedom of group members it is definitely beneficial to be in the same geographic area. As such, a simple and effective way to organize into inner cadre groups is to plot the residences (feel free to use landmarks close to your home if you do not wish to disclose your home address) of those who do not yet have a cadre in to a map and setting up the groups based on local regions. Perhaps some group members live on the north side of town and some live central. Utilizing this method ensures real life meet-ups are more convenient and should the need for mutual aid arise, your fellow inner-cadre members will be able to reach you quickly.

### Shared interests

Another effective means of organizing into inner-cadres is to group people who have shared interests. For example, if several of the middle cadre members are parents wanting to homeschool their children, that would be a natural group to build an inner cadre around. Or if some members wanted to improve their health through diet and exercise, those people could form an inner cadre with some of their goals centered around eating right and fitness. A great way tactic you can use for this method is to use the tool we have created in Appendix __. The tool is a document with a list of the various areas and activities an inner cadre might focus on in their work together. Members can indicate which areas or activities are most important to them and the group can use these responses to link people up with like interests.

### Chronological

While this isn't the best way to organize the inner cadre groups, it does simplify things if the group is having trouble organizing itself. The way this method works is that the first eight people to join the group become an inner-cadre, then the next eight to join, then the next eight, and so on.

### Random

Still another way to organize inner cadre groups is randomly. Take everyone who is involved, put their names in a hat, and draw the first eight, then the next eight, and so on. This should be used as a method of last resort.

We've just discussed the various ways to organize the inner cadre group, which forms the foundation of the Freedom Cell Network. Next we will explore the best practices and critical activities for making the most of the inner cadre.

## Working with the Inner Cadre

The inner cadre group is comprised of the people with whom you share the most trust and interact with the most within the network. It is critical to be on the same page and have a cohesive vision and strategy.

### Establishing Communication Channels

One of the first things to do once you have assembled your inner cadre group is to establish a communication channel, preferably one that is encrypted and secure from external surveillance. The chat platform of choice for many inner cadre groups is Telegram. The Telegram app can be downloaded on both a smartphone or desktop and can use end to end encryption when chatting with your crew. It is recommended that one of the inner cadre members create a Telegram group and share the invite link with the other members. Once all members have joined this can serve as a communication hub for working together and getting on the same page.

Telegram isn't the only option for encrypted chat groups. Signal is also a popular privacy chat app, however it appears at this time that Telegram is the predominant app used by the Freedom Cell Network.

Other chat apps for consideration that many members have been vetting and reviewing:

[**Status**](http://status.im/)

[**Session**](https://getsession.org/)

[**Briar**](https://briarproject.org/)

If you would like to take your communication to another level, consider setting up not only an encrypted communication channel, but an off-grid communication channel as well. Some groups in the network are utilizing HAM or CBD Radio technology to maintain communication should the internet or phone communication fail.
There is a group on FreedomCells.org called [Amateur Radio Freedom Cell Network](https://freedomcells.org/cells/amateur-radio-freedom-cell-network/). Sign up for the site and join the group if you are looking for advice on how to get started with HAM or CB Radio tech for your group.

It is also important to create a cell on the FreedomCells.org website so you can find other groups and members in your area, engage in discussion with other members, and create events for people in your area. As we will discuss later, having an active presence on the site will aid in the creation of your middle cadre group.

Also remember, once you have a communication channel in place, understand that meeting in person will always be superior than communicating through chat apps. Once you have a place to discuss things, the group should set a regular meeting schedule. This should be an in person meeting whenever possible. We recommend meeting at least once a month and keeping up with one another as often as possible through your chat group and zoom/jitsi/phone calls.

## Create a Vision/Mission Statement

Once you have your communication channels set up and regular meetings scheduled, the next thingthings to do is create a vision and mission statement for your group. This statement will help the group to maintain focus and have a cohesive direction that all members can get behind.

Note: Many groups/cells will be born from the main. For example, your inner cadre could have people who comprise the Communications Cell, IT Cell, Gardening Cell, etc. So a vision statement for each group may need to be created. A Communications Cell vision statement may to focus on alternatives comms, test, implement, and share with the other cells. To stay current on technologies and improve communications within the FC community.

The vision and mission statement can help set the direction of where your goals take you. Think of the vision as being what you will be closer to attaining as you accomplish your goals.

Here's an example for a fictional inner cadre called the "South Austin Scorpions"...

**Vision** - *A free and connected South Austin where like minded people trade with one another and provide mutual aid when their community is in need.*

**Mission** - *By meeting regularly, establishing barter networks and trade routes, and working together on common goals, the South Austin scorpions will create a more free community for ourselves and our children to enjoy.*

Having a mission can help the group stay on course should some of the members push to engage in activities outside of the scope of the mission statement. The other members can remind them that their mission involves A, B, and C and what they want to do, D, is not in line with the mission. The group can either expand their mission or the members that want to engage in activities outside of the mission can form a new single purpose inner cadre group to operate alongside the existing one.

Remember, adherence to the mission does not have to be rigid or controlling. It is simply meant as a way to keep people on the same page so the effectiveness of the group does not get "spread thin".

## Establish Common Goals

Now that you have comms, regular meetings planned, and a common vision and mission, it's time to start working on common goals. This is a fundamental role of the inner cadre groups. With 8 people working together it amplifies your effectiveness and efficiency. Working together on common goals is one of the most important functions of Freedom Cell groups. Goals can be accomplished through the inner cadre group and as we will discuss later, at the middle cadre and even the meta cadre level. The decentralized cooperative nature of the freedom cell structure creates an environment where creatively, effectiveness, and efficiency are maximized for both the individual and the group.

There are a variety of different goals and themes that groups can work on. A great place to start are goals aimed at helping to organize the inner cadre activities.

Examples of goals for that purpose are the following...

1. Find around 8 people to form an inner cadre group.
2. Set up a communication channel (preferably encrypted).
3. Create a mission and vision statement.
4. Establish and work on common goals.

Well look at that! You and your group have already accomplished a handful of goals. I'm a big fan of the idea of small victories that build confidence and momentum. The first three goals were listed previous to this section so going in to goal setting your group can already have some accomplishments under your belt. See? This isn't so hard!

The next set of recommended goals are a high priority due to the regular occurrence of civil unrest and the vulnerability of centralized systems throughout the world. The theme for this goal set is preparedness.

**Acquire three months worth of food storage** - Working with a group makes this goal more attainable as inner cadre members can chip in on bulk food storage purchases.

**Self Defense** - *Note: We believe in self defense, but each cell will make their own decisions on how to defend and protect themselves. Some will choose firearms, others will choose self-defense classes, and still others will chooose pacicifism. Choose what works for you and your community.*

If interested in firearms, all group members could have firearms and know how to use them safely and proficiently. Inner cadre members can agree on a couple of common calibers so they can do bulk ammos buys.

**Bugout Plan** - All group members have a bug out plan, whether bug out together or separate, and a bug out bag.

Many inner cadre groups start with this important goal set and branch out from there based on their group needs or their unique vision and mission.

Below are some different themes and goals you can work on with your inner cadre group. You can pick and choose and different areas can overlap. A word of caution however, don't take on too much at once. Start with a limited number or goals, find success with those, and then take on more.

### Homeschool Coop
- Chip in and buy curriculum
- Rotating teachers / Group learning
- Group field trips
- Parent support group

### Parenting
- Rotating childcare
- Weekly play date
- Group outings
- Parent support group

### Spiritual Growth
- Weekly meetings
- Group yoga, meditation, bible readings
- Pilgrimages, group trips
- Accountability Partners

### Health and Wellness
- Formulating diet and exercise plan
- Exercising together
- Chipping in on exercise equipment
- Accountability Partners

### Political
- Research and pick candidates / policies
- Pooling money for supplies
- Lobbying efforts
- Group outreach

### Entrepreneurship
- Hiring staff to work w group members
- Lending circle
- Group trips to conferences
- Accountability partners / Mastermind

### Intentional Community
- Find Your Partners
- List important factors
- Research legal structure
- Create dynamic agreement

## Organizing the Middle Cadre

Now that the inner cadre groups are humming along, it's time for participants to start building out the middle meta cadre. The middle meta cadre is the next level of organization in the Freedom Cell Network where eight inner cadre groups link up and work together on mutual aid and common goals. With greater numbers at play, the effectiveness and creativity of the network is amplified.

With the inner cadres intended to be as hyper local as possible, the middle cadre group branches out across a city or local region perhaps. Once there is a middle cadre group of around 64 people (eight groups of eight), the network can begin setting goals and supporting one another on a larger scale.

Examples of useful activities for the middle cadre group include the following:

**Food exchange network** - For those who prepare food products or grow food, you can exchange with each other. Connect with local farmers and arrange bulk food purchases, share farm pick ups, etc.

**Trade routes** - The middle cadre group in the Dallas / Ft. Worth area is working to establish trade routes where a waypoint is established that serves as a drop point for goods. A member of the group that lives far east for example can drive to a location in the center of the region and drop something off for someone that lives in the opposite side of the area. A member of the Ozark group raises pigs and other members within driving distance can easily arrange for pick-up and purchase and coordinate with other cells too.

**Skill share / social meetups** - Create a skill sheet for members and share it with the core group. This gives a snap shot of what each member brings to the table. The Tulsa Freedom Cell created a sheet that included:
Name, preferred contact info, alternate contact info, business (if applicable), expert skills, intermediate skills, novice skills, skills to learn, payments accepted. They hold regular meetings at a coffeeshop or a member's home.

**Garden-blitzes** - Help each other build gardens. The Tulsa Freedom Cell had a garden blitz had a members house. One brought in compost, and a truckload of mulch was delivered (mulch was free from the city's green waste site, delivery was a small fee), and all who had them brought tools: wheelbarrows, pitchforks, shovels, rakes, etc. The 20' x 25' bed was knocked out in 2.5 hours. The rest of the time was spent eating and socializing.

**Mutual aid support** - Draw on each other's skills and support the network.

**Mutual defense** - Create a defense plan for various scenarios including how to reach other quickly and simultaneously. Get the group on Cell411 and/or the alt comms discussed earlier.

**Business / Trade Listings** - Use the skills to sheet to include members' businesses and use them as much as possible especially if accepting alternative payments such as crypto/trade/barter, etc.
Tulsa Freedom Cell member created a Craigslist type classifieds for their local network.
It is hosted on a private server and requires registation. It is not searchable on the internet and is designed to be an invite only process.

**Establish a communication channel for the middle cadre.**

## Advanced Organizing

1 1 1 1 1 1 1 1 (8) - foundational cell - inner cadre

1 - choose one delegate for meta cadre council

8 8 8 8 8 8 8 8 (64) - middle cadre - eight groups of eight

8 - middle cadre council

1 - middle cadre council chooses one delegate for meta cadre council

64 64 64 64 64 64 64 64 (544) - meta cadre - 8 groups of 64

8 - meta cadre council chosen by middle cadre councils

**From here meta cadres network together form the Confederation of Freedom Cells.**

## Resources

### Security Culture

*Security culture when building cells and meeting new people.*

- [What Is Security Culture](https://crimethinc.com/2004/11/01/what-is-security-culture)
- [Anarchist Conversion Kit - Resources for talking about & understanding State Power](https://docs.google.com/spreadsheets/d/10eBlQngXoaqlXIVkkNGL0ztrY0k2kTca1di-EEe7uMs/edit)

### Food Storage & Purchasing Resources

- [Food Storage Calculator](https://providentliving.com/preparedness/food-storage/foodcalc/)
- [The Survival Podcast](https://www.azurestandard.com/)
- [Azure Standard](https://store.churchofjesuschrist.org/usa/en/food-storage-3074457345616678849-1?pageView=grid&fromPage=catalogEntryList&beginIndex=0)
- [The Survival Mom - Food Storage and Bulk Items](https://thesurvivalmom.com/how-to-storing-food-in-buckets/)
- Connect with and support your local farmers and local farm co-ops.

### Decision making in the Freedom Cell Network

*We recommend learning about a variety of decision making styles to see what works best for
your cell. We believe [Sociocracy](https://www.sociocracyforall.org) to be a great fit for Freedom Cells.*

- [Formal Consensus by CT Butler](http://consensus.net/ocac2.html) (best for inexperienced/newly forming groups)
- [Introduction to Consensus](http://resources.iifac.org/introduction-to-consensus/)
