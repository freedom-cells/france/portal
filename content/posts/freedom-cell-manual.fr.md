---
title: "Manuel des Cellules de Liberté"
url: /manuel-des-cellules-de-liberte/
aliases:
- /fr/freedom-cell-manual/
- /manuel-des-cellules-de-liberté/
# date: 2021-12-16T09:45:03+01:00
tags:
- Guide
showToc: true
TocOpen: true
description: "Notez que nous apprenons au fur et à mesure et que certaines stratégies peuvent mieux fonctionner pour certains groupes que pour d'autres. L'un des objectifs du réseau des Cellules de Liberté est d'être un organisme vivant qui s'autocorrige et évolue. Au fur et à mesure que les individus et les groupes apprennent les meilleures pratiques, ils sont encouragés à les partager avec le réseau dans son ensemble afin que les autres cellules puissent en tirer des enseignements et devenir plus efficaces."
pdf: /freedom-cell-manual.fr.pdf
---

## Qu'est-ce qu'une Cellule de Liberté ?

Les Cellules de Liberté sont des petits groupes de 8 à 10 personnes qui œuvrent ensemble pour s'entraider, atteindre des objectifs communs et garantir la souveraineté des membres du groupe.

Les cellules s'associent ensuite à d'autres groupes dans leur région pour créer un réseau de taille moyenne (cadre intermédiaire), puis ces réseaux s'associent pour créer un plus grand réseau (méta-cadre).

Au fur et à mesure que le réseau s'agrandit, sa capacité à soutenir les membres participants et à atteindre des objectifs communs augmente également.

L'espoir est que le réseau Freedom Cell soit un jour suffisamment grand et efficace pour devenir un véritable moyen d'organisation sociale.

## Principes du réseau des Cellules de Liberté

1. Localisé
2. Décentralisé
3. Axé sur les solutions
4. Apolitique
5. Non violent

## Pourquoi les Cellules de Liberté sont-elles nécessaires ?

Introduction du livre de Derrick Broze, [How to Opt Out of the Technocratic State](https://theconsciousresistance.com/howto/) :

> Alors que l'humanité entre dans la deuxième décennie du XXIe siècle, nous nous trouvons au bord d'une ère technocratique où l'intelligence artificielle (IA), les technologies intelligentes et l'internet des objets font partie de la vie quotidienne. Cette technologie présente des avantages mais a un coût — les entreprises, les gouvernements, les forces de l'ordre et les pirates informatiques sont tous capables de s'immiscer dans nos vies à tout moment. Les entreprises et les gouvernements apprennent même à utiliser la technologie d'une manière qui leur permet d'être les "ingénieurs sociaux" de la société. Le concept de crédit social devient également de plus en plus populaire, et la probabilité que les citoyens subissent des conséquences négatives pour avoir choisi de parler de sujets controversés ou de critiquer les autorités ne fera qu'augmenter.
>
> Cette évolution vers un monde où la technologie numérique est la solution à tout est portée par le secteur de la technologie, plus précisément par les institutions souvent appelées Big Wireless et Big Tech. Les PDG des sociétés transnationales et leurs partenaires gouvernementaux se sont efforcés d'intégrer la technologie numérique dans tous les aspects de l'humanité. Le monde qu'ils envisagent est celui où les scientifiques et les technologues constituent l'élite qui décide de l'avenir de la société. Si la technologie numérique de ces industries n'est apparue qu'au cours des dernières décennies, la philosophie qui guide bon nombre des personnalités de l'industrie et du gouvernement est vieille de près d'un siècle. Cette philosophie d'une domination par des experts technologiques et des scientifiques est connue sous le nom de technocratie. Comme nous le verrons dans les chapitres suivants, les idées qui sous-tendent cette école de pensée influencent discrètement les dirigeants mondiaux depuis des décennies.
>
> Cette obscure théorie politique du XXe siècle est-elle la force motrice de l'évolution vers une dystopie numérique ? Quelles sont les implications d'un monde qui est toujours branché et sur le "réseau" ? Comment préserver la vie privée et la liberté dans une société fondée sur la surveillance de masse, le contrôle technologique et la perte d'individualité ?

## Organiser les Cellules de Liberté

**Inner Cadre** (interne, intérieur, personnel)<br>
8 - 1 1 1 1 1 1 1 1

**Middle Cadre** (intermédiaire, méta-cadre local)<br>
64 - 8 8 8 8 8 8 8 8

**Meta Cadre** (méta-cadre régional)<br>
512 - 64 64 64 64 64 64 64 64

**Confédération des Cellules de Liberté**<br>
4096 - 512 512 512 512 512 512 512 512

### Le cadre interne (Inner Cadre)

Le fondement du réseau Freedom Cell est basé sur des petits groupes de huit personnes appelés cadres internes ou intérieurs. Les membres de ces groupes s'associent volontairement les uns aux autres et s'engagent à se soutenir mutuellement par l'aide et la réciprocité. Les groupes devraient créer une vision et un énoncé de mission, établir des objectifs communs et se réunir régulièrement, au moins une fois par mois, pour faire le point.

### Pourquoi huit personnes ?

Le chiffre huit provient des travaux de John David Garcia et de Bob Podolsky. En 2005, Garcia a publié un livre intitulé [Creative Transformation : A Practical Guide for Maximizing Creativity](http://see.org/garcia/e-ct-dex.htm). Garcia pense que la créativité est l'une des caractéristiques les plus importantes qu'un individu ou un groupe puisse rechercher. Après de nombreuses recherches, Garcia a découvert que huit personnes serait le nombre optimal pour qu'un groupe soit le plus créatif.

> Les couples constituent généralement un groupe trop restreint. Il leur est facile d'accepter des délires communs. Pourtant, on est mieux en couple que seul. Les groupes de plus de huit personnes commencent rapidement à créer une hiérarchie où les membres ont du mal à se traiter avec le même respect et à communiquer à un niveau personnel, d'âme à âme. Cela devient presque impossible pour les groupes de plus de quinze personnes. Les groupes plus importants devraient être divisés en groupes plus petits, de préférence de huit personnes chacun. Par conséquent, pour que le processus soit optimisé, il devrait impliquer quatre hommes et quatre femmes qui se choisissent librement et acceptent d'œuvrer ensemble pour se transformer de manière créative. Les personnes qui ont des difficultés à créer leurs propres groupes de huit peuvent s'attendre à recevoir de l'aide pour former des groupes de huit de la part de ceux qui ont déjà commencé le processus. [http://see.org/garcia/e-ct-5.htm](http://see.org/garcia/e-ct-5.htm)

Ce nombre de huit est un objectif que nous nous efforçons d'atteindre dans le réseau des Cellules de Liberté, mais il n'est pas absolu. L'objectif est de faire en sorte que le groupe cadre interne soit aussi proche que possible de huit personnes. Et si une répartition entre hommes et femmes est idéale, ce n'est pas non plus un absolu.

## 12 conseils pour construire des Cellules de Liberté

Du livre de Derrick Broze, [How to Opt Out of the Technocratic State](https://theconsciousresistance.com/howto/) :

> En conclusion, je vous propose ces "**12 conseils pour construire des Cellules de Liberté**" comme point de départ pour lancement de votre groupe. Veuillez les adapter aux besoins spécifiques de votre communauté :
>
> 1. Comprendre votre motivation - Je trouve qu'il est précieux pour toute personne qui envisage de créer une cellule, un cercle ou un centre de savoir pourquoi elle poursuit cet objectif. Quelles sont vos motivations et vos intérêts ? En sachant cela avant de créer un groupe, vous gagnerez du temps. Trouver des moyens de se retirer de la technocratie est un objectif évident, mais qu'est-ce qui vous motive d'autre ?
> 2. Identifiez les candidats potentiels - Sont-ils mentalement, physiquement et spirituellement aptes à atteindre vos objectifs ?
> 3. Discutez des thèmes communs - Quelles sont les forces motrices qui rassemblent le groupe ?
> 4. Identifiez les forces et les faiblesses - Examinez honnêtement les forces et les faiblesses de chaque individu ainsi que du groupe dans son ensemble.
> 5. Évaluer le niveau de liberté souhaité par rapport à la sécurité - Chaque individu peut avoir un niveau de liberté souhaité différent et, en tant que tel, il aura besoin de plus de liberté et une acceptabilité des risques différents. Lorsqu'il s'agit de la technocratie, il est particulièrement important de s'en souvenir. Quel niveau de liberté souhaitez-vous vraiment voulez-vous vraiment avoir ? Quel degré de confidentialité souhaitez-vous conserver ? Que ferez-vous pour atteindre un tel objectif ?
> 6. Fixez des objectifs à court et à long terme - Que peut accomplir votre cellule en trois mois ? Six mois ? Un an ? Fixez des objectifs en tant que groupe et tenez-vous mutuellement responsables.
> 7. Formation à la pleine conscience - Incorporez des pratiques comme la formation à la communication non violente et la méditation de groupe dans votre cellule.
> 8. Atteindre les objectifs - Documentez chaque objectif atteint avec succès par la cellule ou les membres individuels.
> 9. Formation continue du groupe, communication - Développez continuellement les connaissances de votre cellule, les compétences, et les fournitures.
> 10. Promouvoir/Marketer les objectifs et les réalisations - Utilisez le pouvoir des médias sociaux (quand ils sont sûrs) et le marketing pour faire savoir au monde entier comment la cellule s'est comportée et témoigner de la prospérité de la contre-économie.
> 11. Identifier des stratégies pour créer des revenus/une indépendance - Exploitez le pouvoir et le nombre de votre cellule pour créer des revenus contre-économiques qui ne peuvent pas être taxés.
> 12. Réseau avec d'autres cellules - La clé pour sortir de la technocratie est de construire la communauté contre-économique. Cela signifie non seulement votre communauté immédiate d'alliés, mais aussi le réseau plus large de cellules dans votre ville, votre région/province ou votre pays, et la communauté globale. Il vous appartient de de faire l'effort de vous mettre en réseau avec d'autres activistes et libres penseurs.

## Organiser le cadre interne (Inner Cadre)

Dans cette section, nous allons aborder la formation du cadre interne à partir de trois points de départ différents.

1. Un individu sans personne dans un groupe.
2. Un groupe de 2 à 6 personnes.
3. Un groupe de plus de huit personnes.

Ensuite, nous partagerons quelques conseils importants sur la culture de la sécurité, en particulier sur la manière de s'assurer que vous ne travaillez pas en étroite collaboration avec un agent du système ou une personne perturbée qui nuit à la progression et à la cohésion du groupe.

### Un individu sans personne dans un groupe - Loup solitaire

Le travail du "loup solitaire" pour former un groupe de cadre interne est peut-être le plus difficile. La première étape, si ce n'est déjà fait, est de s'inscrire sur le site Web des Freedom Cells à l'adresse [https://freedomcells.org](https://freedomcells.org). De là, vous pourrez utiliser les cartes des membres et des cellules pour voir s'il y a des membres ou une cellule dans votre région. Si c'est le cas, envoyez-leur un message et/ou rejoignez la cellule. Mais tout le monde n'a pas ce luxe.

S'il n'y a pas déjà quelqu'un dans votre région, c'est à vous de commencer à construire le réseau Freedom Cell à partir de zéro. Félicitations !

### Voici par où commencer

Commencez par les personnes que vous connaissez déjà et en qui vous avez confiance et qui sont sur la même longueur d'onde. Pensez à des cercles concentriques avec vous au centre du cercle, puis les personnes les plus proches de vous dans le cercle suivant, celles que vous connaissez dans le cercle suivant, et les personnes que vous n'avez rencontrées que quelques fois dans le cercle suivant. Commencez par les cercles intérieurs.

*N'oubliez pas que tous les membres du réseau Freedom Cell ne sont pas forcément des anarchistes radicaux et ne partagent pas les mêmes opinions à 100%.*

Si vous avez dans votre entourage quelqu'un qui s'intéresse à la préparation, au jardinage ou qui parle négativement du système et du statu quo, cette personne peut être un bon point de départ.

Si vous ne connaissez personne qui pourrait vous convenir, il existe des endroits mûrs pour le recrutement dans la plupart des communautés. Numériquement, vous pouvez utiliser Meetup.com ou Facebook.com pour trouver des rencontres ou des groupes dans votre région qui partagent les mêmes idées. Vous pouvez commencer par des rencontres libertaires, des groupes de préparation/survie, des jardiniers et des agriculteurs, des groupes d'enseignement à domicile, des groupes de responsabilisation de la police et tout groupe qui s'efforce de sortir du statu quo ou qui en a clairement marre et cherche des solutions.

Créez un MeetUp ou un autre groupe sur les médias sociaux pour attirer les gens. Par exemple, un groupe de volontaires et/ou d'agoristes, ou un groupe de jardinage, de mise en conserve, de préparation, etc. pour votre région. Utilisez ce groupe comme plateforme de recrutement. Organisez régulièrement des réunions axées sur des sujets liés au volontariat et à l'agorisme. Cela créera un bon réservoir de personnes à ajouter à votre cellule après vérification. Gardez ces réunions séparées de celles de votre cadre interne. Elles devraient être accueillantes pour le public et les sujets discutés en conséquence.

### Autres ressources pour créer des groupes

Nextdoor app * Telegram

Recherchez des groupes et des activités dans votre région qui correspondent à vos intérêts et qui pourraient s'intégrer à une cellule (jardinage, permaculture, radio HAM ou CB, conservation des aliments, etc).

Un autre bon endroit pour rencontrer des gens est le marché fermier local. Vous y trouverez des gens qui sortent au moins des sentiers battus en ce qui concerne l'origine de leur nourriture, une facette clé de la vie indépendante.

Une fois que vous avez trouvé des personnes avec lesquelles vous pourriez collaborer dans le cadre de cette effort, présentez-vous. Apprenez à les connaître. Essayez de construire une "**valeur partagée, basée sur des valeurs partagées**". Une fois que vous êtes à l'aise les uns avec les autres, faites-leur savoir que vous cherchez à développer de petits groupes d'entraide et demandez-leur s'ils seraient intéressés à faire partie de votre groupe.

Sachez que ce processus d'établissement de la confiance et de recherche des bonnes personnes à partir de rien peut prendre du temps. Soyez patient et ayez confiance que les bonnes personnes entreront dans votre vie.

Notez cependant que si vous rencontrez des difficultés dans une petite ville ou dans une région où les gens ne valorisent généralement pas la liberté, même si c'est une étape difficile, envisagez de déménager dans un endroit où il y a une plus grande concentration de personnes qui valorisent la liberté. Le site Freedom Cells serait un excellent endroit pour rechercher la meilleure région où vous pourriez déménager.
Vous pouvez vous mettre en relation de manière préventive avec d'autres membres des Freedom Cells et obtenir des conseils sur les bonnes régions et sur ce à quoi il faut faire attention lorsque vous vous installez dans leur région.

### Un groupe de 2 à 6 personnes

Si votre groupe compte déjà deux personnes ou plus, mais pas encore huit, vous pouvez utiliser plusieurs des méthodes énumérées ci-dessus pour recruter vos derniers membres. Vous pouvez multiplier votre efficacité en divisant les parties de la ville ou les différents domaines d'intérêt. Par exemple, un membre du groupe peut se concentrer sur le recrutement dans les marchés de producteurs tandis qu'un autre membre peut commencer à établir des relations avec les groupes locaux d'enseignement à domicile. Le fait d'avoir plus d'une personne dans votre groupe facilitera l'ajout de nouvelles personnes, car celles qui envisagent de se joindre au groupe verront que l'idée a déjà le vent en poupe.

Cependant, une autre stratégie qui permettrait de faire progresser rapidement le développement d'un cadre moyen (huit groupes d'environ huit personnes) dans votre région serait que chacune de ces personnes dans votre groupe se fixe comme objectif de recruter sept personnes. À cet égard, chacun des premiers membres forme une sorte de cellule de recrutement où ils travaillent ensemble, partageant les meilleures pratiques, tout en s'adressant individuellement à leurs propres réseaux dans l'espoir de construire plusieurs cellules dès le départ.
Un groupe de moins de huit personnes peut également exploiter la carte des membres de FreedomCells.org pour tenter d'établir des liens avec d'autres membres de FC dans votre région.

### Un groupe de plus de huit personnes

Il est bon d'avoir plus de huit personnes dans un groupe lorsque vous commencez cette aventure. Lorsque vous avez plus de 8 personnes, vous avez essentiellement le début du cadre intermédiaire (8 groupes de 8) déjà en place. À partir de là, vous pouvez travailler à la construction des groupes du cadre intermédiaire. Maintenant, la question qui se pose naturellement est la suivante : "comment se diviser en deux groupes ou plus à partir de là ?". Rappelez-vous, comme John David Garcia l'a correctement souligné, "les groupes de plus de huit personnes commencent rapidement à créer une hiérarchie dont les membres ont du mal à se traiter avec un respect égal et à communiquer à un niveau personnel, d'âme à âme".

Veillez à ce que chacun comprenne l'objectif de la division du grand groupe en petits groupes. Cela permettra d'amplifier la créativité et d'accroître l'efficacité de la communication et de l'entraide. Sachez également que l'ensemble du groupe restera connecté par le biais du groupe de cadre intermédiaire, même si les membres des cadres internes sont encouragés à nouer des liens plus étroits entre eux qu'avec le groupe de cadre intermédiaire.

## Méthodes d'organisation

Il existe plusieurs méthodes à utiliser pour décider comment diviser des groupes plus importants en groupes plus petits. Voici une brève liste et une description de quatre méthodes que vous pouvez utiliser pour organiser l'encadrement intermédiaire. Notez que certaines de ces méthodes peuvent être plus efficaces pour certains groupes que pour d'autres et qu'il est possible que vous trouviez que plusieurs stratégies sont nécessaires pour organiser votre réseau de cadres intermédiaires.

### Familiarité et confiance

L'une des meilleures façons d'organiser des cadres internes est de se lier à ceux avec qui vous avez déjà une relation familière et de confiance. Ainsi, vous pouvez passer directement aux choses sérieuses sans avoir à les "contrôler" ou à vous préoccuper des questions de sécurité. Cependant, cela ne fonctionne pas toujours pour l'ensemble du groupe, car tout le monde ne se connaît pas forcément de près. Dans ce cas, les personnes qui se connaissent bien ou qui font partie de la famille peuvent créer des cadres internes et le groupe dans son ensemble peut utiliser l'une des autres méthodes pour organiser le reste du groupe de cadre intermédiaire.

### Proximité géographique

Bien que les groupes du cadre interne ne doivent pas nécessairement être des groupes de "rencontre en personne" et peuvent être virtuels, pour atteindre les objectifs et protéger la liberté des membres du groupe, il est définitivement bénéfique de se trouver dans la même zone géographique. Ainsi, un moyen simple et efficace de s'organiser en groupes de cadres internes consiste à reporter sur une carte les résidences (n'hésitez pas à utiliser des points de repère proches de votre domicile si vous ne souhaitez pas divulguer votre adresse) de ceux qui n'ont pas encore de cadre et à créer des groupes en fonction des régions locales. Peut-être que certains membres du groupe vivent dans le nord de la ville et d'autres dans le centre. En utilisant cette méthode, les rencontres dans la vie réelle sont plus pratiques et, en cas de besoin d'aide mutuelle, les membres de votre cadre seront en mesure de vous joindre rapidement.

### Intérêts communs

Un autre moyen efficace de s'organiser en cercles internes est de regrouper les personnes qui ont des intérêts communs.
Par exemple, si plusieurs des membres du cadre intermédiaire sont des parents qui souhaitent faire l'école à la maison à leurs enfants, ce serait un groupe naturel pour créer un cadre interne. Ou si certains membres souhaitent améliorer leur santé par le biais de l'alimentation et de l'exercice physique, ces personnes pourraient former un cadre interne dont certains des objectifs seraient centrés sur l'alimentation et la forme physique. Une excellente tactique que vous pouvez utiliser pour cette méthode est l'outil que nous avons créé à l'annexe __. Cet outil est un document contenant une liste des différents domaines et activités sur lesquels un cadre interne pourrait se concentrer dans son travail. Les membres peuvent indiquer quels domaines ou activités sont les plus importants pour eux et le groupe peut utiliser ces réponses pour mettre en relation des personnes ayant des intérêts similaires.

### Chronologique

Bien que ce ne soit pas la meilleure façon d'organiser les groupes de cadres internes, cela simplifie les choses si le groupe a du mal à s'organiser. Cette méthode fonctionne de la manière suivante : les huit premières personnes à rejoindre le groupe deviennent un cadre interne, puis les huit suivantes, puis les huit suivantes, et ainsi de suite.

### Aléatoire

Une autre façon d'organiser les groupes de cadres internes est le tirage au sort. Prenez toutes les personnes concernées, mettez leurs noms dans un chapeau, et tirez les huit premiers, puis les huit suivants, et ainsi de suite. Cette méthode ne doit être utilisée qu'en dernier recours.

Nous venons de discuter des différentes façons d'organiser le groupe de cadre interne, qui constitue la base du réseau des Cellules de Liberté. Nous allons maintenant explorer les meilleures pratiques et les activités essentielles pour tirer le meilleur parti de ce groupe.

## Travailler avec le cadre interne (Inner Cadre)

Le groupe du cadre interne est composé des personnes avec lesquelles vous avez le plus confiance et avec lesquelles vous interagissez le plus au sein du réseau. Il est essentiel d'être sur la même longueur d'onde et d'avoir une vision et une stratégie cohérentes.

### Etablir des canaux de communication

L'une des premières choses à faire une fois que vous avez réuni votre groupe interne est d'établir un canal de communication, de préférence chiffré et protégé de la surveillance extérieure. La plateforme de messagerie instantanée choisie par de nombreux groupes de cadres internes est Telegram. L'application Telegram peut être téléchargée sur un smartphone ou un ordinateur et peut utiliser un chiffrement de bout en bout lors des discussions avec votre équipe. Il est recommandé que l'un des membres du cadre interne crée un groupe Telegram et partage le lien d'invitation avec les autres membres. Une fois que tous les membres ont rejoint le groupe, celui-ci peut servir de centre de communication pour travailler ensemble et être sur la même longueur d'onde.

Telegram n'est pas la seule option pour les discussions chiffrées. Signal est également une application de chat très populaire, mais il semble pour l'instant que Telegram soit l'application la plus utilisée par le réseau Freedom Cell.
D'autres applications de chat à prendre en considération ont été examinées et évaluées par de nombreux membres :

[**Status**](http://status.im/)

[**Session**](https://getsession.org/)

[**Briar**](https://briarproject.org/)

Si vous souhaitez porter votre communication à un autre niveau, envisagez de mettre en place non seulement un canal de communication chiffré, mais aussi un canal de communication hors réseau. Certains groupes du réseau utilisent la technologie HAM ou CBD Radio pour maintenir la communication en cas de panne d'Internet ou du téléphone.
Il existe un groupe sur FreedomCells.org appelé [Amateur Radio Freedom Cell Network](https://freedomcells.org/cells/amateur-radio-freedom-cell-network/). Inscrivez-vous sur le site et rejoignez le groupe si vous cherchez des conseils sur la façon de commencer à utiliser la technologie HAM ou CB Radio pour votre groupe.

Il est également important de créer une cellule sur le site FreedomCells.org pour pouvoir trouver d'autres groupes et membres dans votre région, engager des discussions avec les autres membres et créer des événements pour les personnes de votre région. Comme nous le verrons plus tard, le fait d'avoir une présence active sur le site facilitera la création de votre groupe de cadre intermédiaire.

N'oubliez pas non plus qu'une fois que vous aurez mis en place un canal de communication, sachez qu'une rencontre en personne sera toujours préférable à une communication par le biais d'applications de chat. Une fois que vous avez un endroit pour discuter, le groupe devrait établir un calendrier de réunions régulières. Dans la mesure du possible, il devrait s'agir d'une réunion en personne. Nous vous recommandons de vous réunir au moins une fois par mois et de rester en contact les uns avec les autres aussi souvent que possible par le biais de votre groupe de discussion et d'appels téléphoniques/zoom/jitsi.

## Créer une vision/un énoncé de mission

Une fois que vous avez mis en place vos canaux de communication et programmé des réunions régulières, la prochaine chose à faire est de créer une vision et une déclaration de mission pour votre groupe. Cette déclaration aidera le groupe à rester concentré et à avoir une direction cohérente à laquelle tous les membres peuvent adhérer.

Note : de nombreux groupes/cellules naîtront de l'essentiel. Par exemple, votre cadre interne pourrait être composé de personnes qui constituent la cellule de communication, la cellule informatique, la cellule de jardinage, etc. Il peut donc être nécessaire de créer une déclaration de vision pour chaque groupe. La déclaration de vision d'une cellule de communication peut se concentrer sur les communications alternatives, les tester, les mettre en œuvre et les partager avec les autres cellules. Rester à jour sur les technologies et améliorer les communications au sein de la communauté FC.

Les énoncés de vision et de mission peuvent aider à définir la direction dans laquelle vos objectifs vous mènent. Pensez à la vision comme étant ce que vous serez plus près d'atteindre au fur et à mesure que vous accomplirez vos objectifs.

Voici un exemple pour un cadre intérieur fictif appelé les "South Austin Scorpions"...

**Vision** - _Un Sud d'Austin libre et connecté où les personnes partageant les mêmes idées échangent entre elles et s'entraident lorsque leur communauté est dans le besoin._

**Mission** - _En se réunissant régulièrement, en établissant des réseaux de troc et des routes commerciales, et en travaillant ensemble sur des objectifs communs, les scorpions d'Austin Sud créeront une communauté plus libre pour nous et nos enfants._

Le fait d'avoir une mission peut aider le groupe à garder le cap si certains membres poussent à s'engager dans des activités en dehors du cadre de la déclaration de mission. Les autres membres peuvent leur rappeler que leur mission implique A, B et C et que ce qu'ils veulent faire, D, n'est pas conforme à la mission. Le groupe peut soit élargir sa mission, soit les membres qui veulent s'engager dans des activités en dehors de la mission peuvent former un nouveau groupe de cadre interne à but unique qui fonctionnera parallèlement au groupe existant.

N'oubliez pas que l'adhésion à la mission ne doit pas être rigide ou contrôlante. Il s'agit simplement d'un moyen de garder les gens sur la même longueur d'onde afin que l'efficacité du groupe ne soit pas "dispersée".

## Etablir des objectifs communs

Maintenant que vous avez des communications, des réunions régulières planifiées, une vision et une mission communes, il est temps de commencer à travailler sur des objectifs communs.
Il s'agit là d'un rôle fondamental des groupes du cadre interne. Avec 8 personnes travaillant ensemble, vous amplifiez votre efficacité et votre efficience.

Travailler ensemble sur des objectifs communs est l'une des fonctions les plus importantes des Cellules de Liberté. Les objectifs peuvent être atteints par le biais du groupe interne et, comme nous le verrons plus tard, au niveau du cadre intermédiaire et même du méta-cadre. La nature coopérative décentralisée de la structure des Cellules de Liberté crée un environnement où la créativité, l'efficacité et l'efficience sont maximisées tant pour l'individu que pour le groupe.

Il existe une variété d'objectifs et de thèmes différents sur lesquels les groupes peuvent travailler. Un excellent point de départ est constitué par les objectifs visant à aider à organiser les activités du cadre interne.

Voici des exemples d'objectifs à cette fin...

1. Trouver environ 8 personnes pour former un groupe "cadre interne".
2. Etablir un canal de communication (de préférence chiffré).
3. Créer une déclaration de mission et de vision
4. Etablir et travailler sur des objectifs communs

Eh bien, regardez ça ! Vous et votre groupe avez déjà accompli une poignée d'objectifs. Je suis un grand partisan de l'idée de petites victoires qui renforcent la confiance et l'élan. Les trois premiers objectifs ont été énumérés avant cette section, donc avant de fixer des objectifs, votre groupe peut déjà avoir des réalisations à son actif. Vous voyez ? Ce n'est pas si difficile !

La prochaine série d'objectifs recommandés est hautement prioritaire en raison de l'occurrence régulière de troubles civils et de la vulnérabilité des systèmes centralisés dans le monde entier. Le thème de cette série d'objectifs est la préparation.

**Acquérir l'équivalent de trois mois de stockage de nourriture** - Travailler en groupe rend cet objectif plus réalisable, car les membres du groupe peuvent participer à l'achat de nourriture en gros.

**Autodéfense** - Nous croyons en l'autodéfense, mais chaque cellule prendra ses propres décisions sur la façon de se défendre et de se protéger. Certains choisiront des cours d'autodéfense et d'autres encore le pacifisme. Choisissez ce qui fonctionne pour vous et votre communauté.

**Plan de repli (Bugout Plan)** - Tous les membres du groupe ont un plan de sortie de crise, qu'ils sortent ensemble ou séparément, et un sac d'évacuation.

De nombreux groupes d'encadrement interne commencent par cet objectif important et se diversifient ensuite en fonction des besoins du groupe ou de leur vision et mission uniques.

Vous trouverez ci-dessous quelques thèmes et objectifs différents sur lesquels vous pouvez travailler avec votre groupe d'encadrement interne. Vous pouvez faire votre choix et différents domaines peuvent se chevaucher. Mais attention, n'en faites pas trop à la fois. Commencez par un nombre limité d'objectifs, réussissez-les, puis passez à d'autres.

### Coopérative d'enseignement à domicile
- Participez et achetez le programme d'études
- Rotation des enseignants / Apprentissage en groupe
- Sorties de groupe
- Groupe de soutien aux parents

### Formation des parents
- Garde d'enfants à tour de rôle
- Rencontres de jeux hebdomadaires
- Sorties de groupe
- Groupe de soutien aux parents

### Croissance spirituelle
- Réunions hebdomadaires
- Groupe de yoga, méditation, lectures bibliques
- Pèlerinages, voyages de groupe
- Partenaires de responsabilisation

### Santé et bien-être
- Formulation d'un régime alimentaire et d'un plan d'exercice
- Faire de l'exercice ensemble
- Participation à l'achat de matériel d'exercice
- Partenaires de responsabilité

### Politique
- Recherche et choix des candidats / politiques
- Mise en commun de l'argent pour les fournitures
- Efforts de lobbying
- Sensibilisation des groupes

### Esprit d'entreprise
- Embauche de personnel pour travailler avec les membres du groupe
- Cercle de prêt
- Voyages de groupe pour assister à des conférences
- Partenaires de responsabilité / Mastermind

### Communauté intentionnelle
- Trouvez vos partenaires
- Liste des facteurs importants
- Recherche de la structure juridique
- Créer un accord dynamique

## Organiser le cadre intermédiaire (Middle Cadre)

Maintenant que les groupes du cadre interne fonctionnent bien, il est temps pour les participants de commencer à construire le méta-cadre intermédiaire. Le méta-cadre intermédiaire est le niveau d'organisation suivant du réseau des Cellules de Liberté, où huit groupes du cadre interne se réunissent et travaillent ensemble sur l'aide mutuelle et les objectifs communs. Avec un plus grand nombre de personnes en jeu, l'efficacité et la créativité du réseau sont amplifiées.

Les cadres internes étant destinés à être aussi locaux que possible, le groupe de cadre intermédiaire se ramifie à travers une ville ou une région locale. Une fois que le groupe de cadre intermédiaire compte environ 64 personnes (huit groupes de huit), le réseau peut commencer à fixer des objectifs et à se soutenir mutuellement à plus grande échelle.

Voici quelques exemples d'activités utiles pour le groupe de cadre intermédiaire :

**Réseau d'échange alimentaire** - Pour ceux qui préparent des produits alimentaires ou cultivent des aliments, vous pouvez échanger entre vous. Etablissez des liens avec les agriculteurs locaux et organisez des achats de nourriture en vrac, partagez des collectes à la ferme, etc.

**Routes commerciales** - le groupe de cadre intermédiaire de la région de Dallas / Ft. Worth travaille à l'établissement de routes commerciales où un point de passage est établi et sert de point de chute pour les marchandises. Un membre du groupe qui vit loin à l'est, par exemple, peut se rendre à un endroit situé au centre de la région et déposer quelque chose pour quelqu'un qui vit à l'autre bout de la zone. Un membre du groupe Ozark élève des porcs et les autres membres qui se trouvent à une certaine distance en voiture peuvent facilement organiser le ramassage et l'achat et se coordonner avec d'autres cellules également.

**Partage de compétences / rencontres sociales** - Créez une fiche de compétences pour les membres et partagez-la avec le groupe principal. Cela donne un aperçu de ce que chaque membre apporte à la table. La Cellule de Liberté de Tulsa a créé une fiche comprenant les éléments suivants :
Nom, coordonnées préférées, coordonnées alternatives, entreprise (le cas échéant), compétences expertes, compétences intermédiaires, compétences novices, compétences à apprendre, paiements acceptés. Ils tiennent des réunions régulières dans un coffeeshop ou au domicile d'un membre.

**Jardins éclairs** - S'entraider pour construire des jardins. La Cellule de Liberté de Tulsa a organisé un "Graden-Blitz" chez un membre. L'un d'entre eux a apporté du compost, et un camion de paillis a été livré (le paillis était gratuit depuis le site des déchets verts de la ville, la livraison était payante), et tous ceux qui en avaient ont apporté des outils : brouettes, fourches, pelles, râteaux, etc. Le lit de 20' x 25' a été réalisé en 2 heures et demie. Le reste du temps a été consacré aux repas et à la convivialité.

**Soutien mutuel** - Faites appel aux compétences des autres et soutenez le réseau.

**Défense mutuelle** - Créez un plan de défense pour divers scénarios, y compris la façon de joindre les autres rapidement et de manière simultanée. Rejoignez le groupe sur Cell411 et/ou sur les communications alternatives discutées précédemment.

**Listes d'entreprises / de commerce** - Utilisez les compétences à la feuille pour inclure les entreprises des membres et les utiliser autant que possible, surtout si vous acceptez des paiements alternatifs tels que crypto/troc (trade/barter), etc.
Un membre de la Cellule de Liberté Tulsa a créé un site de petites annonces de type Craigslist pour son réseau local.
Il est hébergé sur un serveur privé et nécessite une inscription. Il n'est pas consultable sur Internet et est conçu pour être un processus sur invitation uniquement.

**Etablir un canal de communication pour les cadres intermédiaires.**

## Organisation avancée

1 1 1 1 1 1 1 1 (8) - cellule de base - cadre interne

1 - choisir un délégué pour le conseil du méta-cadre

8 8 8 8 8 8 8 8 (64) - cadre intermédiaire - huit groupes de huit

8 - conseil du cadre intermédiaire

1 - le conseil du cadre intermédiaire choisit un délégué pour le conseil du méta-cadre

64 64 64 64 64 64 64 64 (512) - méta-cadre - 8 groupes de 64

8 - conseil du méta-cadre choisi par les conseils des cadres intermédiaires

**A partir de là, le réseau des méta-cadres forme la Confédération des Cellules de Liberté.**

## Ressources

### Culture de la sécurité

*Culture de la sécurité lors de la construction de cellules et de la rencontre de nouvelles personnes.*

- [What Is Security Culture](https://crimethinc.com/2004/11/01/what-is-security-culture)
- [Anarchist Conversion Kit - Resources for talking about & understanding State Power](https://docs.google.com/spreadsheets/d/10eBlQngXoaqlXIVkkNGL0ztrY0k2kTca1di-EEe7uMs/edit?usp=sharing)

### Ressources pour le stockage et l'achat de nourriture

- [Food Storage Calculator](https://providentliving.com/preparedness/food-storage/foodcalc/)
- [The Survival Podcast](https://www.azurestandard.com/)
- [Azure Standard](https://store.churchofjesuschrist.org/usa/en/food-storage-3074457345616678849-1?pageView=grid&fromPage=catalogEntryList&beginIndex=0)
- [The Survival Mom - Food Storage and Bulk Items](https://thesurvivalmom.com/how-to-storing-food-in-buckets/)
- Entrez en contact avec vos agriculteurs locaux et vos coopératives agricoles locales et soutenez-les.

### Prise de décision dans le réseau Freedom Cell

*Nous vous recommandons de vous renseigner sur une variété de styles de prise de décision pour voir ce qui fonctionne le mieux pour votre cellule. Nous pensons que la [Sociocratie](https://www.sociocracyforall.org) convient aux Cellules de Liberté.*

- [Formal Consensus by CT Butler](http://consensus.net/ocac2.html) (pour les groupes inexpérimentés/nouveaux groupes en formation)
- [Introduction to Consensus](http://resources.iifac.org/introduction-to-consensus/)
